# **PageObjects Integration Tests Tutorial** for Confluence

Shows you how to write integration tests for a Confluence plugin.

This repository is associated to the tutorial [Writing integration tests for Confluence using PageObjects](https://seuqra.atlassian.net/wiki/display/ACDD/Tutorial+-+Writing+integration+tests+for+Confluence+using+PageObjects)
