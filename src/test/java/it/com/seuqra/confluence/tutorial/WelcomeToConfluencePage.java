package it.com.seuqra.confluence.tutorial;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;


public class WelcomeToConfluencePage extends ConfluenceAbstractPage {
	private static final String PREFIX = "/display/ds/"; 
	private static final String PAGE = "Welcome+to+Confluence"; 

	@ElementBy(id = "WelcometoConfluence-WithConfluenceitiseasytocreate,editandsharecontentwithyourteam.Chooseatopicbelowtostartlearninghow.")
	protected PageElement welcomeTitle;
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}

	public TimedQuery<String> getWelcomeTitle() {
		return welcomeTitle.timed().getText();
	}

}
