package it.com.seuqra.confluence.tutorial;

import static com.atlassian.pageobjects.elements.query.Poller.*;
import static org.hamcrest.Matchers.*;

import org.hamcrest.Matcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceLoginPage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.TestedProductFactory;

public class TestWelcomeToConfluencePage {
	private static ConfluenceTestedProduct confluenceTestedProduct = TestedProductFactory.create(ConfluenceTestedProduct.class);
	
	
	@BeforeClass
	public static void login() {
		// WORKAROUND: Method ConfluenceAbstractPage.doWait() raises always and exception because the condition "return typeof window.ajsScriptsFinishedLoading != 'undefined'" is never true
		// When setupComplete variable is false, this condition is never checked :)
		ConfluenceAbstractPage.setSetupComplete(false);
		ConfluenceLoginPage confluenceLoginPage = confluenceTestedProduct.gotoLoginPage();
		confluenceLoginPage.login("admin", "admin", true);
	}
	
	@AfterClass
	public static void logout() {
		confluenceTestedProduct.logOut();
	}
	
	@Test
	public void testWelcomePageTitle() {
		Page page = confluenceTestedProduct.getPageBinder().navigateToAndBind(WelcomeToConfluencePage.class);
		WelcomeToConfluencePage welcomeToConfluencePage = (WelcomeToConfluencePage) page;
		waitUntil(welcomeToConfluencePage.getWelcomeTitle(), (Matcher<String>) containsString("With Confluence it is easy to create, edit and share content with your team."));
	}
	
}
