package ut.com.seuqra.confluence.tutorial;

import org.junit.Test;
import com.seuqra.confluence.tutorial.MyPluginComponent;
import com.seuqra.confluence.tutorial.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}