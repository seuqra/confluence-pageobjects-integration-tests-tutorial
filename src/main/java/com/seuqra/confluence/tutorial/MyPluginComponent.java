package com.seuqra.confluence.tutorial;

public interface MyPluginComponent
{
    String getName();
}